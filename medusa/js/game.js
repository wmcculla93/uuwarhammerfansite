		var score = 0, 
			speed = 10000,
			lives = 4,
			zindex = 800,
			hiScores, arrayPlace;
			
		var bgmusic = new Audio("bin/music.mp3"),
			metal = new Audio("bin/metal.wav"),
			scoresound = new Audio("bin/scoresound.wav");
			
		//  Spawns enemy and begins animation
		function spawnNid() {
		
				var leftPos = Math.floor((Math.random() * 80) + 10),
				soundfx = Math.floor((Math.random() * 5) + 1),
				spawnChance = Math.floor((Math.random() * 100));
				
				// Chance to spawn is 20% every 0.3 of a second adding random aspect to game
				if (spawnChance > 80)
				{
					// Add enemy to container
					$('#gameCont').append('<img src="img/nid.gif" class="nid nidS" style="left:'+ leftPos +'%; z-index:' + zindex + '">');
					zindex--;
					
					// Begin animation, if animation completes, player takes damage
					$('.nid').animate({ "width": "80%", "left": "17.5%", "top" : "20%"}, speed, "linear", function() {
						
						$('#bloodslash').show(0).delay(1000).hide(0);		

						$(this).remove();
						
						lives--;
						
						// If player has no lives remaining, game over, else take 1 damage
						if (lives == 0)
						{
							gameOver();
						}
						else {
							$("#skull" + lives).hide();
							var painSound = new Audio("bin/pain.mp3");
							painSound.play();						
						}
					});
					
					// if the speed is greater than half a second to animation complete, speed up every spawn
					if (speed > 500)
					{
						speed -= 100;
					}
					
					// Play random enemy sound effect
					var music = new Audio("bin/nidsound/" + soundfx + ".wav");
					music.play();
				}
		}
		
		// Changes score text so that it always has at least 6 characters
		function leadingZeros(num){
			var leading = num+"";
			var zero = "0";
			
			while(leading.length < 6)
			{
				leading = zero.concat(leading);
			}			
				
			return leading;
		}
		
		
		// Reset and start game
		function gameStart() {
			
			timer = window.setInterval(function () {spawnNid();}, 300);
			score = 0;
			speed = 10000;
			lives = 4;
			zindex = 800;
			$("img.nid").remove();
			var scoreText = leadingZeros(score);
			$('#score').text(scoreText);
			$(".skullCounter").show();
			bgmusic.play();
			
		}
		
		// Stops game if user changes tab
		function gameChangeTab(){
			if (typeof timer !== 'undefined')
			{
				clearInterval(timer);
			}
			bgmusic.pause();
			$('.nidS').stop(true);
			$("#gameContDiv > div").hide();
			$("#mainMenu").show();
		}
		
		// Game over, carried out when player loses all lives
		function gameOver() {
			resetGO();
			clearInterval(timer);
			var death = new Audio("bin/dead.wav");
			death.play();
			bgmusic.pause();
			$('.nidS').stop(true);
			$("#gameCont").fadeOut(4000);
			$("#gameOverCont").fadeIn(4000, function() {
			
				var kills = score/1000;
				
				
				metal.play();
				$("#gameOverKills td:first-child").show(0, function() {
				
				var killCount = 0;
				$("#gameOverKills td:nth-child(2)").show();
				
				
				// Animated score counter
				scoreTimer = window.setInterval(function () {	
					
					if (killCount != kills)
					{
						killCount++;
						scoresound.play();
						$("#gameOverKills td:nth-child(2)").text(killCount);
					}
					else
					{
						clearInterval(scoreTimer);
						
						metal.play();
						$("#gameOverScore td:first-child").show(0, function() {
						
							var scoreCount = 0;
							$("#gameOverScore td:nth-child(2)").show();
							
							scoreTimer = window.setInterval(function () {
						
							if (scoreCount != score)
							{
								scoreCount += 500;
								scoresound.play();
								$("#gameOverScore td:nth-child(2)").text(scoreCount);	
							}
							else
							{
								hiScoreCheck();
								clearInterval(scoreTimer);
							}
							
							}, 100);
							
						});
					}
					
					}, 100);
				});							
			});
		}
		
		function getHighScores() {
			$.getJSON("bin/highscores.json", function(data) {				
					
				if (localStorage.hiscores === undefined) {
				
					// save default high scores to local storage
					localStorage.setItem("hiscores", JSON.stringify(data));
								
				}
				
				var hiScoreRaw = $.parseJSON(localStorage.hiscores);
				
				// map local storage data to array
				hiScores = $.map(hiScoreRaw, function(hiScoreData) {return hiScoreData;});
				
			});
					
		}
		
		// action carried out if user has high score
		function highScoreGot() {
			$('#goTitle').text("High Score!");
			$("#gameOverKills").hide();
			$("#hiScorePanel").fadeIn(1000);
			arrayPlace = i;
		}
		
		// check is user has gained a high score
		function hiScoreCheck() {
			
					var highScoreGot = false;
					
					for (var i = 0; i < hiScores.length; i++)
					{
						if (hiScores[i]["score"] < score)
						{
							highScoreGot = true;
							$('#goTitle').text("High Score!");
							$("#gameOverKills").hide();
							$("#hiScorePanel").fadeIn(1000);
							arrayPlace = i;
							break;
						}
					}

					if (highScoreGot == false) {
						$("#gameOverButs").fadeIn(1000);
					}					
		}
		
		// event handler for user clicking hiscore enter button
		$("#hiScoreReturn").click(function() {
			hiScores.splice(arrayPlace, 0, {"name": $("#hiName").val(), "score":score});
			hiScores.pop();
			
			// updates local storage with new score
			localStorage.hiscores = JSON.stringify(hiScores);
			
			$('#goTitle').text("High Score Entered!");
			$("#hiScorePanel").hide();
			$("#gameOverButs").fadeIn(1000);
		});
		
		// Splash screen displayed when player begins new game
		function splashScreen(){
			
			$("#gameSplash p:first-child").fadeIn(1000, function() {			
				$(this).next().delay(3000).fadeIn(1000, function() {
					$(this).next().delay(1000).fadeIn(1000, function() {
						$(this).next().delay(1000).fadeIn(1000).delay(3000).show(0, function() {
							$('#gameSplash').fadeOut(500);
							$('#gameCont').fadeIn(1000);			
							gameStart();
						});
					});	
				});
			});	
			
		}
		
		// Resets game over scores
		function resetGO() {
			$('#gameOverCont').find("div, td").hide();
			$("#goTitle").text("Only in death does duty end...");
			$("#gameOverScore td:nth-child(2)").text("0");
			$("#gameOverKills td:nth-child(2)").text("0");
		} 
	
		$(document).ready(function (e) {
		
			var posX = 0,
			posY = 0;
	
			getHighScores();
			
			// Event handler to handle crosshair on mousemove
			$('#gameCont').mousemove(function (e) { 
			
				posX = $(this).offset().left;
				posY = $(this).offset().top;
			
				var vX = ((e.pageX - posX) - ($('#vBar').width() * 0.5));
					vY = ((e.pageY - posY) - ($('#vBar').height() * 0.5));
					hX = ((e.pageX - posX) - ($('#hBar').width() * 0.5));
					hY = ((e.pageY - posY) - ($('#hBar').height() * 0.5));
				
				$('#vBar').css( "left", vX + "px");
				$('#vBar').css( "top", vY + "px");
				
				$('#hBar').css( "left", hX + "px");
				$('#hBar').css( "top", hY + "px");
			});
			
			// Event handler to handle user firing
			$('#gameCont').click(function (e) { 
				$("#firing").show(1).delay(50).hide(1);
				$("#cocking").hide(1).delay(50).show(1).delay(50).hide(1);

				var fire = new Audio("bin/fire.wav");
				fire.play();
			});			
				
			// Event handler to handle user hitting enemy
			$('#gameCont').on("click", ".nid", function (e) {
					bX = ((e.pageX - posX));
					bY = ((e.pageY - posY) - $('#blood').height());
				
				$('#blood').css( "left", bX + "px");
				$('#blood').css( "top", bY + "px");
				$('#blood').show().delay(1000).hide(0);
				
				$(this).stop(true);	
				$(this).animate({ "width": "0%", "left" : "80%"}, 1000, function() { $(this).remove(); });
				$(this).removeClass('nid');
				
				score += 1000;
				var scoreText = leadingZeros(score);
				$('#score').text(scoreText);
			});
			
			
			// Event handler for clicking start button
			$('#playBut').click(function() {
		
				$('#mainMenu').fadeOut(500);
				$('#gameSplash').fadeIn(1000);
				$('#gameSplash').find("p").hide();			
				splashScreen();
			
			});	

			// Event handler for player clicking high score button
			$('#highBut').click(function() {
			
					$('#scores').empty();					
					
					$('#scores').append("<tr class=title><td>Name</td><td>Score</td></tr>");
			
					// Print high scores to table
					for (var i = 0; i < hiScores.length; i++)
					{
						$('#scores').append("<tr><td>" + hiScores[i]["name"] + "</td><td>" + hiScores[i]["score"] + "</td></tr>");
					}
					
					$('#mainMenu').fadeOut(500);			
					$('#scoreDiv').fadeIn(1000);				
			});
			
			// Event handler for playing sound when mouse over buttons
			$('.menuBut').mouseover(function() {
				var mouseHover = new Audio("bin/menuHover.mp3");
				mouseHover.play();
			});
			
			// Event handler for score button click
			$('#scoreMenuBut').click(function() {
			
				$('#scoreDiv').fadeOut(500);
				$('#mainMenu').fadeIn(1000);
				
			});
			
			// Event handler for try again button on game over
			$('#goTryAgain').click(function() {
				$('#gameOverCont').fadeOut(500);
				$('#gameCont').fadeIn(1000);			
				gameStart();
			});
		
			// Event handler for game over 'return to menu' button
			$('#goMenu').click(function() {
				$('#gameOverCont').fadeOut(500);
				$('#mainMenu').fadeIn(1000);			
			});
			
			// event handler for Skip button during splash screen
			$('#skipBut').click(function() {
				$('#gameSplash').children().stop(true);
				$('#gameSplash').fadeOut(500);
				$('#gameCont').fadeIn(1000);			
				gameStart();
			});
		});
		