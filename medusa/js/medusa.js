		var planetInfo;
		
		// Action carried out when user selects new planet
		function selectPlanet(selVal) {
			
				$(".selectedLi").removeClass("selectedLi");				
				$('[listID=' + selVal + ']').addClass("selectedLi");
				
				$('#planetImg').attr('src', ("img/" + planetInfo[selVal]['img']));
				$('#planetName').text(planetInfo[selVal]['name']);
				$('#planetSubTitle').text(planetInfo[selVal]['class']);
				$('#planetDesc').text(planetInfo[selVal]['desc']);
				$('#planetPop').text(planetInfo[selVal]['pop']);
				$('#planetTithe').text(planetInfo[selVal]['tithe']);
				
		}

		$(document).ready(function() {
		
			// get data from json, set user selections and initial view
			$.getJSON("bin/medusa.json", function(data) {
						
				// map JSON data to array
				planetInfo = $.map(data, function(planetInfo) {return planetInfo;});
				var first = true;
				
				for (var i = 0; i < planetInfo.length; i++)
				{
					var appendString = "<li listID=" + i;
					
					if (first == true)
					{
						appendString += " class=selectedLi";
						first = false;
					}
					
					appendString += ">" + planetInfo[i]['name'] + "</li>";
					
					$('#planetList').append(appendString);
					$('#selPlan').append("<option value=" + i + ">" + planetInfo[i]['name'] + "</option>");
				}
				
				$('planetImg').attr('src', ("img/" + planetInfo[0]['img']));
				$('#planetName').text(planetInfo[0]['name']);
				$('#planetSubTitle').text(planetInfo[0]['class']);
				$('#planetDesc').text(planetInfo[0]['desc']);
				$('#planetPop').text(planetInfo[0]['pop']);
				$('#planetTithe').text(planetInfo[0]['tithe']);
			});	
			
			// event handler for clicking planet title
			$('#planetList').on("click", "[listID]", function() {
			
				var selectedLi = $(this).attr('listID');
				selectPlanet(selectedLi);
			});
			
			// event handler for changing drop down menu
			$('#selPlan').change(function() {
				selectPlanet($('#selPlan').val());
			});
		});