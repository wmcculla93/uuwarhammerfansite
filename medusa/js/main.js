var beep = new Audio("bin/beep.wav"),
		textTimer,
		videoTimer,
		counter = 1,
		maxCounter = 0;
		
		// Start Gallery animation
		$(document).ready(function() {
			galleryAnimation();
			maxCounter = $('[galleryId]').length;
			
			videoTimer = window.setInterval(function() {
				var videoChance = Math.floor((Math.random() * 100) + 1);
				if (videoChance > 50)
				{
					$('#videoPrompt').slideToggle(500);
					textTimer = window.setInterval(function() {textInOut();}, 500);
					clearInterval(videoTimer);
				}
			}, 1000);
			
			// Close video player
			$('#videoBut').click(function()
			{
				$('#monitorVid').trigger('pause');
				$('#videoPlayer').slideToggle(2000);
			});
		
		
			// Closes prompt and opens player
			$('#videoPrompt').click(function()
			{
				clearInterval(textTimer);
				$('#videoPrompt').slideUp(500, function() {
					$('#videoPlayer').slideToggle(2000, function() {
						$('#monitorVid').trigger('play');
					});
				});
			});
			
			// event handler for header menu buttons
			$('.headerButton').click(function() {
				gameChangeTab();
			
				var tab = $(this).attr('tar');
				
				$('.currentTab').hide();
				$(tab).show();
			
				$('.currentTab').removeClass('currentTab')
				$(tab).addClass('currentTab');
			
				$('.selected').removeClass('selected');
				$(this).addClass('selected');
			});
		});
		
		// Gallery animation
		function galleryAnimation() {
			$('[galleryId=' + counter + '] .galleryImage').animate({ "left": "55%"}, 7000, "linear");
			$('[galleryId=' + counter + '] .spa1').animate({ "left": "25%"}, 7000, "linear");
			$('[galleryId=' + counter + '] .spa2').animate({ "left": "30%"}, 7000, "linear");
			$('[galleryId=' + counter + '] .spa3').animate({ "left": "27.5%"}, 7000, "linear", function() {
			
				$('[galleryId=' + counter + ']').fadeOut(3000, function() {
				});
				
				counter++;
					
				if (counter > maxCounter)
				{
					counter = 1;
				}
					
				$('[galleryId=' + counter	 + ']').fadeIn(3000, function() {
					$('[galleryId] > span, [galleryId] > img').removeAttr('style');
					galleryAnimation();
				});
				
			});
		}
		
		// Causes text to fade out and in during video prompt (also plays sound)
		function textInOut() {
			$('#videoIncoming').fadeToggle(500);
			beep.play();
		}