# Warhammer 40K Fansite - Uni Project

## Features

This project was for a university project at Ulster University. Its essientially a fansite for Warhammer 40K, showcasing some
jQuery animation and design. The main meat of the project was a game that I designed ('Genestealer Hunt') with a short narrative,
sound effects, increasing difficulty and score keeping. Hope you enjoy!

## Limitations

One of the main limitations of this project (bearing in mind, it was designed at uni several years ago) is that it is not responsive.
Once you step outside of the 1920x1080 resolution, things start getting a bit weird with the design.

## Technologies Used

Languages used: JS, CSS, HTML

IDE: Notepad++